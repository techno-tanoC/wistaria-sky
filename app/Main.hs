{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import Config

import Control.Monad.IO.Class
import Data.Aeson (FromJSON)
import GHC.Generics
import Web.Scotty
import Network.Wai.Middleware.Cors

data Push = Push {
  name :: String,
  url :: String,
  ext :: String
} deriving (Show, Generic)

data Cancel = Cancel {
  id :: String
} deriving (Show, Generic)

instance FromJSON Push
instance FromJSON Cancel

main :: IO ()
main = do
  conf <- readConfig

  scotty (port conf) $ do
    middleware simpleCors

    get "/" $ do
      json True

    options "/cancel" $ return ()
    post "/push" $ do
      Push n u e <- jsonData
      return ()

    options "/cancel" $ return ()
    post "/cancel" $ do
      Cancel i <- jsonData
      return ()
