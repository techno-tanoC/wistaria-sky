module Config where

import Data.Maybe (maybe, fromMaybe)
import System.Environment (lookupEnv)

data Config = Config {
  port :: Int,
  dest :: String
} deriving Show

readConfig :: IO Config
readConfig = do
  p <- maybe 8989 read `fmap` lookupEnv "PORT"
  d <- fromMaybe "/sky" `fmap` lookupEnv "DEST"
  return $ Config p d
