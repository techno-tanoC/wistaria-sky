module WistariaSky.ProgressSpec where

import WistariaSky.Progress

import Control.Concurrent.MVar
import Test.Hspec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Progress" $ do
    it "set content-length" $ do
      pg <- newMVar $ newProgress "test"
      p <- readMVar pg
      contentLength p `shouldBe` 0
      setCL pg 100
      p <- readMVar pg
      contentLength p `shouldBe` 100

    it "progresses aquired" $ do
      pg <- newMVar $ newProgress "test"
      p <- readMVar pg
      aquired p `shouldBe` 0
      progress pg 100
      p <- readMVar pg
      aquired p `shouldBe` 100
      progress pg 100
      p <- readMVar pg
      aquired p `shouldBe` 200
