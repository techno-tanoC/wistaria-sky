module WistariaSky.TrackerSpec where

import WistariaSky.Tracker

import Control.Concurrent
import Control.Concurrent.MVar
import Test.Hspec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Trackers" $ do
    it "starts tracking" $ do
      ts <- newTrackers
      one <- newMVar ()
      two <- newMVar ()

      a <- newMVar True
      startTracker ts a $ do
        takeMVar one
        putMVar two ()

      putMVar one ()
      collect ts `shouldReturn` [True]
      takeMVar two

      threadDelay (100 * 1000)
      collect ts `shouldReturn` []

    it "cancelled tracking" $ do
      ts <- newTrackers
      one <- newMVar ()
      two <- newMVar ()

      a <- newMVar True
      id <- startTracker ts a $ do
        takeMVar one
        putMVar two ()

      putMVar one ()
      cancel ts (show id)
      threadDelay (100 * 1000)
      collect ts `shouldReturn` []
