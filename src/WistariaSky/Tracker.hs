module WistariaSky.Tracker where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception (finally)
import Data.Traversable (traverse)
import qualified Data.Map.Strict as M

type Key = String

data Tracker a = Tracker {
  content :: MVar a,
  threadId :: ThreadId
}

readContent :: Tracker a -> IO a
readContent = readMVar . content

readKey :: Tracker a -> Key
readKey = show . threadId

cancelTracker :: Tracker a -> IO ()
cancelTracker = killThread . threadId

type Trackers a = MVar (M.Map Key (Tracker a))

newTrackers :: IO (Trackers a)
newTrackers = newMVar M.empty

collect :: Trackers a -> IO [a]
collect mvar = do
  ts <- readMVar mvar
  traverse readContent $ M.elems ts

insert :: Tracker a -> Trackers a -> IO ()
insert t var = modifyMVarMasked_ var $ \ts ->
  return $ M.insert (show . threadId $ t) t ts

delete :: Key -> Trackers a -> IO ()
delete k var = modifyMVarMasked_ var $ \ts ->
  return $ M.delete k ts

startTracker :: Trackers a -> MVar a -> IO () -> IO ThreadId
startTracker ts a f = forkIO $ finally first after
  where
    first = do
      my <- myThreadId
      insert (Tracker a my) ts
      f
    after = do
      my <- myThreadId
      delete (show my) ts

cancel :: Trackers a -> Key -> IO ()
cancel var k = do
  ts <- readMVar var
  case M.lookup k ts of
    Just t -> cancelTracker t
    Nothing -> return ()
